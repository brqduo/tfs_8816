import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  LoginPage,
  LoginService,
  UtilService,
  TokenService,
  ErrorService,
  StorageService,
  ImageService,
  SecurityService,
  NetWorkService,
  EnvironmentService,
  UserService,
  OnsPackage} from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics';


const serviceLoginArr = [
  LoginService,
  UtilService,
  TokenService,
  ErrorService,
  StorageService,
  ImageService,
  SecurityService,
  NetWorkService,
  EnvironmentService,
  UserService,
]

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    OnsPackage.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule
  ],


  providers: [
    StatusBar,
    SplashScreen,
    ...serviceLoginArr,
    AnalyticsService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
