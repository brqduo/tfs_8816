import { EmailInterface } from './email.interface';
import { TelefoneInterface } from './telefone.interface';

export interface ContatoInterface {
    agente: string;
    cargo: string;
    data_nascimento: string;
    email: EmailInterface[];
    foto: string;
    nome: string;
    ons: boolean;
    perfil: string;
    telefone: TelefoneInterface[];
    tipoUsuario: number;
    id: string;
    type: string;
}