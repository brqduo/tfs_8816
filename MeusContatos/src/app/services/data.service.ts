import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subject, ReplaySubject, Observable } from 'rxjs';
import { Contato } from 'src/models/contato';
import * as _ from 'lodash';
import { componentNeedsResolution } from '@angular/core/src/metadata/resource_loading';
import { FiltroArray } from 'src/models/filtrosArray.interface';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  public contatos: any = [];
  public contatosData: ReplaySubject<any>;
  public txt = new Observable();
  private _FiltrosObj = [
    {
      nome: 'Cargo',
      dados: []
    },
    {
      nome: 'Perfil',
      dados: []
    },
    {
      nome: 'Gerencia',
      dados: []
    },
    {
      nome: 'Gerencia Executiva',
      dados: []
    },
    {
      nome: 'Instituicao',
      dados: []
    }

  ];



  constructor(public firestore: AngularFirestore) {
    this.getMudancas();
    this.contatosData = new ReplaySubject<any>();

  }

  getMudancas() {
    const dados = this.firestore.collection('contatos2');
    // dados.doc('pviana').valueChanges().subscribe(x => { return console.log(x)});
    return dados.stateChanges().subscribe(actions => {
      const x = actions.map(a => {
        const data = a.payload.doc.data() as Contato;
        data.id = a.payload.doc.id;
        data.type = a.payload.type;
        return data;
      });
      if (this.contatos.length < 1) {
        this.contatos = x;
      } else {
        x.forEach(item => {

          switch (item.type) {
            case 'modified':
              const objIndex = this.contatos.findIndex((obj => obj.id == item.id));
              this.contatos[objIndex] = item;
              break;
            case 'added':
              this.contatos.push(item);
              break;
            case 'removed':
              const objIndexR = this.contatos.findIndex((obj => obj.id == item.id));
              console.log(objIndex);
              this.contatos.splice(objIndexR, 1);
              break;
            default:
              break;
          }

        });
      }
      this.txt
      this.contatosData.next(this.contatos);
      this.FillFiltroObj();
    });
  }

  FillFiltroObj() {
    const cargos = _.uniq(_.map(this.contatos, 'cargo'));
    const perfil = _.uniq(_.map(this.contatos, 'perfil'));

    console.log('Filtro Cargos', cargos);
    this.FiltroCargo = cargos;
    this.FiltroPerfil = perfil;
    console.log('Filtro Perfil', perfil);

  }

  get FiltroOBj() {
    return this._FiltrosObj;
  }

  get FiltroCargo() {
    return this._FiltrosObj[0].dados;
  }
  set FiltroCargo(cargos: Array<string>) {
    this._FiltrosObj[0].dados = cargos;
  }

  get FiltroPerfil() {
    return this._FiltrosObj[1].dados;
  }
  set FiltroPerfil(perfil: Array<string>) {
    this._FiltrosObj[1].dados = perfil;
  }

  filtroContatos(termoBusca) {
    return this.contatos.filter(item => {
      return item.nome.toLowerCase().indexOf(termoBusca.toLowerCase()) > -1;
    });

  }
}