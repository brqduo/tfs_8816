import { FiltroArray } from './../../../models/filtrosArray.interface';
import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.page.html',
  styleUrls: ['./filtros.page.scss'],
})
export class FiltrosPage implements OnInit {
  FiltrosArr = null;
  termoBusca: string;
  FiltrosList: Array<string> = [];
  constructor(private dataService: DataService) {
    this.FiltrosArr = this.dataService.FiltroOBj;
    for (let i = 0; i < this.FiltrosArr.length; i++) {
      const element = this.FiltrosArr[i];
      element.selected = false;
    }
    console.log('array da linha 16', this.FiltrosArr);

  }

  ngOnInit() {
  }

  showFilter(ev: FiltroArray) {
    console.log(ev);

    for (let i = 0; i < this.FiltrosArr.length; i++) {
      const element = this.FiltrosArr[i];
      element.selected = false;
    }
    if (ev.selected) {
      ev.selected = false;
      this.FiltrosList = [];
      console.log('Dentro do seleted true');
      
    } else {
      console.log('Dentro do seleted false');
      ev.selected = true;
      this.FiltrosList = ev.dados;
    }

  }



}
