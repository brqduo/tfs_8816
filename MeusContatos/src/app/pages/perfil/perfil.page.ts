import { DataService } from '../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ContatoInterface } from 'src/models/contato.interface';

@Component({
  selector: 'app-perfil',
  templateUrl: 'perfil.page.html',
  styleUrls: ['perfil.page.scss']
})
export class PerfilPage implements OnInit {
  contatos: ContatoInterface[] = [];
  constructor(private dataService: DataService) {
    console.log('ENTREI NA TAB3');
    this.dataService.contatosData.subscribe(data => {
      console.log('ENTREI NA TAB3 dentro da função subscribe');
      for (let i = 0; i < data.length; i++) {
        const element = data[i];

        if (i <= 6) {
          this.contatos.push(element);
        }
      }
    });
  }



  ngOnInit() {

  }
}
