import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';
import { ContatoInterface } from 'src/models/contato.interface';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  contatos: ContatoInterface[] = [];
  constructor(private dataService: DataService) {
    console.log('ENTREI NA TAB1');

    /* this.contatos = this.dataService.contatos; */
    this.dataService.contatosData.subscribe(data => {
      console.log('ENTREI NA TAB1 dentro da função subscribe');
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        if (i <= 4) {
          this.contatos.push(element);
        }
      }
    });


  }


  ngOnInit() {
    /*     this.dataService.contatosData.subscribe(data => {
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
    
            if (i <= 4) {
              this.contatos.push(element);
            }
          }
        });
        */

  }
}
