import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ItemContatoComponent } from '../components/item-contato/item-contato.component'
import { CardCarrouselComponent } from '../components/card-carrousel/card-carrousel.component';


@NgModule({
  imports: [
    IonicModule,
    CommonModule
  ],
  declarations: [
    CardCarrouselComponent,
    ItemContatoComponent
  ],
  exports: [
    CardCarrouselComponent,
    ItemContatoComponent
  ]
})
export class SharedModule { }
