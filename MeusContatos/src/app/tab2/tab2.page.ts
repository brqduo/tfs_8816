import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormControl } from "@angular/forms";
import { debounceTime } from "rxjs/operators";
import { ContatoInterface } from 'src/models/contato.interface';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
  contatos: ContatoInterface[] = [];
  termoBusca: string = "";
 onFilter = false;
  constructor(
    private dataService: DataService,
    private router: NavController) {
    console.log('ENTREI NA TAB2');
    this.contatos = this.dataService.contatos;
    this.dataService.contatosData.subscribe(data => {
      console.log('ENTREI NA TAB2 dentro da função subscribe');
      this.contatos = data;
      console.log(this.contatos[1]);
      console.log(JSON.stringify(this.contatos[1]));
    })
  }

  ngOnInit() {
  

  }
  
  test(eventItem) {
    console.log('chegando do item selecionada na lista:', eventItem);
  }


  goToFilter() {
    this.router.navigateForward('/filtros')
  }

  setFiltroContatos(termo: string) {
    this.contatos = this.dataService.filtroContatos(termo);

    if (termo.length > 0) {
      console.log('termo esta preenchido', termo.length);
      this.onFilter = true;
    } else {
      console.log('termo esta vazio');
      this.onFilter = false;
    }

    console.log('TABS2', termo);
  }

  itemHeightFn(item, index) {
    return 90;
  }
}
