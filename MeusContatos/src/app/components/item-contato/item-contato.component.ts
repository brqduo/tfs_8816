import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IonItem, IonItemSliding } from '@ionic/angular';
import { ContatoInterface } from 'src/models/contato.interface';

@Component({
  selector: 'app-item-contato',
  templateUrl: './item-contato.component.html',
  styleUrls: ['./item-contato.component.scss'],
})
export class ItemContatoComponent implements OnInit {
  @Input() ItemListvalue: ContatoInterface;
  @Input() isSlidable: boolean;
  @Input() buttonForSlide: object;
  @ViewChild('item') IonItem: IonItemSliding;
  @Output() SelectedItem:EventEmitter<ContatoInterface> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    console.log(this.IonItem);
  }


  selecionarItem(item: ContatoInterface) {
    console.log('Resposta para o component pai', this.SelectedItem.emit(item));
  }


}
