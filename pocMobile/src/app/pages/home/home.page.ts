import { DemandaModel } from './../../service/todo.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Todo, TodoService } from '../../service/todo.service';
import { FilterService } from 'src/app/service/filter.service';
import { NgModel } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  items: any;
  todos: Todo[] = [];
  demandaList: DemandaModel[] = [];
  contatoList: DemandaModel[] = [];
  searchTerm = '';
  constructor(public fireStoreCol: AngularFirestore, public filterSrv: FilterService, private todoService: TodoService) {
    this.fireStoreCol.doc('/usuario/Paulo/').valueChanges().subscribe((res) => {
      console.log(res);
    });
  }

  ngOnInit() {
    this.setFilteredItems();
    this.todoService.getTodos().subscribe(res => {
      console.log('RES sw 22', res);
      this.todos = res;
    });

    this.todoService.getDemanda().subscribe((res: DemandaModel[]) => {
      this.getMudancas();
    });

  }
  remove(item) {
    this.todoService.removeTodo(item.id);
  }
  load() {
    this.todoService.loadFakeDemanda();
  }
  getMudancas() {
    const dados = this.fireStoreCol.collection('demanda');
    return dados.stateChanges().subscribe(actions => {
      const ArrayContato = actions.map(a => {
        const data = a.payload.doc.data() as DemandaModel;
        data.id = a.payload.doc.id;
        data.type = a.payload.type;
        return data;
      });
      if (this.demandaList.length < 1) {
        this.demandaList = ArrayContato;
        this.contatoList = this.demandaList;
      } else {
        ArrayContato.forEach(item => {
          switch (item.type) {
            case 'modified':
              const objIndex = this.demandaList.findIndex((obj => obj.id == item.id));
              this.demandaList[objIndex] = item;
              this.contatoList = this.demandaList;
              break;
            case 'added':
              this.demandaList.push(item);
              this.contatoList = this.demandaList;
              break;
            case 'removed':
              const objIndexR = this.demandaList.findIndex((obj => obj.id == item.id));
              console.log(objIndex);
              this.demandaList.splice(objIndexR, 1);
              this.contatoList = this.demandaList;
              break;
            default:
              break;
          }
        });
      }
    });
  }

  setFilteredItems() {
    this.items = this.filterSrv.filterItems(this.searchTerm);
  }

  onSearchInput(ev: string, itemModel: NgModel) {
    console.log(ev);
    console.log(itemModel);
    itemModel.valueChanges.pipe(debounceTime(700)).subscribe((result) => {
      if (result.length === 0 || result === null || result === '') {
        console.log('passei na linha 90');
        this.contatoList = this.demandaList;
      } else {
        console.log('passei na linha 93');
        this.contatoList = this.contatoList.filter(item => {
          return item.nome.toLowerCase().indexOf(result.toLowerCase()) > -1;
        });
      }
    });

  }

}
