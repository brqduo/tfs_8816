import { Injectable } from '@angular/core';
import { DemandaModel } from './todo.service';

@Injectable()
export class FilterService {
    demandaList: DemandaModel[] = [];
    constructor() { }



    filterItems(searchTerm: string): DemandaModel[]  {
        return this.demandaList.filter(item => {
            return item.nome.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
          });
    }
}