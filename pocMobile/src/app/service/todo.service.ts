import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
declare var Chance: any;
export interface Todo {
  id?: string;
  task: string;
  priority: number;
  createdAt: number;
}
export interface DemandaModel {
  id?: any;
  type?: any;
  nome: string;
  email: string;
  foto: string;
  cargo: string;
  testObj?: object;
}
@Injectable()
export class TodoService {
  chance: any;
  private todosCollection: AngularFirestoreCollection<Todo>;
  private todos: Observable<Todo[]>;

  private demandaCollection: AngularFirestoreCollection<DemandaModel>;
  private demanda: Observable<DemandaModel[]>;


  constructor(db: AngularFirestore) {
    this.chance = new Chance();
    this.todosCollection = db.collection<Todo>('todos');
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        console.log('linha 22 do serviço TODO', actions);
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

    this.demandaCollection = db.collection<DemandaModel>('demanda');
    this.demanda = this.demandaCollection.snapshotChanges().pipe(
      map(actions => {
        console.log('linha 45 do serviço TODO', actions);
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  getTodos() {
    return this.todos;
  }

  getDemanda() {
    return this.demanda;
  }
  loadFakeDemanda() {
    let testeDemanda: DemandaModel;
    for (let i = 0; i < 10; i++) {
      testeDemanda = {
        nome: this.chance.name(),
        email: this.chance.email(),
        foto: this.chance.avatar(),
        cargo: '',
        testObj: {
          teste1: 123,
          teste2: 1234
        }
      };
      console.log(testeDemanda);
      this.addDemanda(testeDemanda);
    }
  }
  addDemanda(demanda: DemandaModel) {
    console.clear();
    return this.demandaCollection.add(demanda).then((res) => {
      console.log('finalizado a inclusão', res.id);
    });
  }

  getTodo(id) {
    return this.todosCollection.doc<Todo>(id).valueChanges();
  }
  updateTodo(todo: Todo, id: string) {
    return this.todosCollection.doc(id).update(todo);
  }
  addTodo(todo: any) {
    return this.todosCollection.add(todo);
  }
  removeTodo(id) {
    return this.todosCollection.doc(id).delete();
  }
}