// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: 'AIzaSyBxAZ8u8aRoQ3AffC5D3KVMQsCv8jMql48',
    authDomain: 'firestoretest-649ce.firebaseapp.com',
    databaseURL: 'https://firestoretest-649ce.firebaseio.com',
    projectId: 'firestoretest-649ce',
    storageBucket: 'firestoretest-649ce.appspot.com',
    messagingSenderId: '114666644044',
    appId: '1:114666644044:web:f69f7756110a4bc2'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
