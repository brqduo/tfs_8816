import * as admin from "firebase-admin";
import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/auth';

import * as goChance from 'chance';
const ChanceJS = require('chance');
const chance = new ChanceJS();
const firestore = admin.firestore;
const serviceAccount = require("./credentials.json");
let VezesQueRodou = 0;
export interface DemandaModel {
    agente: string;
    cargo: string;
    data_nascimento: any;
    email: EmailInterface[];
    foto: string;
    nome: string;
    ons: boolean;
    perfil: any;
    telefone: TelefoneInterface[];
    tipoUsuario: number;
    id: any;
    type: string;
}

export interface EmailInterface {
    email: string;
    tipo: string;
}
export interface TelefoneInterface {
    numero: string;
    tipo: string;
}



export interface GDInterface {
    nome: string;
    dados: {
        metas: {
            prodMensal: number;
            horasPrev: number;
            horasEntregues: number;
            metaCumprida: any;
        }
        saldo: any;
        metaSemanal: {
            semana1: {
                valor: number;
                isSemanaAtual: boolean;
            };
            semana2: {
                valor: number;
                isSemanaAtual: boolean;
            };
            semana3: {
                valor: number;
                isSemanaAtual: boolean;
            };
            semana4: {
                valor: number;
                isSemanaAtual: boolean;
            };
            semana5: {
                valor: number;
                isSemanaAtual: boolean;
            };

        };
    };
}




admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://firestoretest-649ce.firebaseio.com"
});


const loadFakeContatos = (() => {
    let testeDemanda: DemandaModel;
    const contatosArr: DemandaModel[] = [];
    console.log('ISSO FUNCIONA!', goChance.Chance().name());
    for (let i = 0; i < 20; i++) {
        testeDemanda = {
            agente: goChance.Chance().name(),
            cargo: goChance.Chance().profession(),
            data_nascimento: new String(goChance.Chance().birthday),
            email: [{
                email: goChance.Chance().email(),
                tipo: goChance.Chance().cc_type(),
            }],
            foto: goChance.Chance().avatar(),
            nome: goChance.Chance().name(),
            ons: goChance.Chance().bool(),
            perfil: goChance.Chance(),
            telefone: [{
                numero: goChance.Chance().phone(),
                tipo: goChance.Chance().cc_type(),
            }],
            tipoUsuario: goChance.Chance().integer(),
            id: goChance.Chance().integer(),
            type: '',

        };
        pushTofireStore(testeDemanda)
    }
    /*     admin.firestore().collection('demanda').add().then((res) => {
    
        })  */
})
const testDashboardGD: GDInterface[] = [{
    
    nome: 'Delphi/.NET/ETL',
    dados: {
        metas: {
            prodMensal: goChance.Chance().hour(),
            horasPrev: goChance.Chance().hour(),
            horasEntregues: goChance.Chance().hour(),
            metaCumprida: ' - ',
        },
        saldo: ' - ',
        metaSemanal: {
            semana1: {
                valor: 95,
                isSemanaAtual: false,
            },
            semana2: {
                valor: 190,
                isSemanaAtual: false,
            },
            semana3: {
                valor: 285,
                isSemanaAtual: false,
            },
            semana4: {
                valor: 380,
                isSemanaAtual: true,
            },
            semana5: {
                valor: 463,
                isSemanaAtual: false,
            },
        }
    }
}]

const pushTofireStore = ((contados: DemandaModel) => {
    VezesQueRodou++
    admin.firestore().collection('demanda').add(contados).then((res) => {
        console.clear();
        console.log('Rodou: ', VezesQueRodou);
    })
})

console.log('eu existo!!!')
console.log('Vem logo abaixo:') 
console.log(JSON.stringify(testDashboardGD))
/* loadFakeContatos() */